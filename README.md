Here you can find 4 tests created with Python and Selenium framework.

Tests resource - https://the-internet.herokuapp.com/.

Tested example - Form Authentication


Set up instruction:

1. Download PyCharm Community - https://www.jetbrains.com/pycharm/download/


In terminal:

2. brew install python

3. brew install --cask chromedriver

4. pip install selenium==3.14.0

5. npm install gecko driver


After you can run main.py

Project pages http://maksimvl.gitlab.io/mercifulwasps-pages/
