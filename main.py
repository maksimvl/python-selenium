from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait

browser = webdriver.Chrome()
browser.get("https://the-internet.herokuapp.com/login")

# 1 test. Assert that we are in login page
assert browser.find_element_by_tag_name('h2').text == "Login Page"

# 2 test. Assert that we had logged in.
def setNamePassword(a,b):
    username = browser.find_element_by_id('username')
    password = browser.find_element_by_id('password')
    submit = browser.find_element_by_class_name('radius')
    username.send_keys(a)
    password.send_keys(b)
    submit.click()

setNamePassword("tomsmith","SuperSecretPassword!")
wait = WebDriverWait(browser, 5)
assert browser.find_element_by_class_name('subheader').text == "Welcome to the Secure Area. When you are done click logout below."

# 3 test. Assert that we had logged out
logout = browser.find_element_by_css_selector('button.secondary, .button.secondary')
logout.click()
assert browser.find_element_by_tag_name('h2').text == "Login Page"

# 4 test. Assert that we had not logged in with wrong password
setNamePassword("tomsmith","123")
wait = WebDriverWait(browser, 5)
flash = browser.find_element_by_xpath('//*[@id="flash"]')
flash_class = flash.get_attribute("class")
assert flash_class == 'flash error'